﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

using UnityEngine.SceneManagement;
public class MonsterController: MonoBehaviour
{
    //Playerの位置を取得する
    public Transform target;

    //敵モンスターを自動的にプレイヤーに向けて移動させる
    NavMeshAgent agent;

    //アニメーターコンポーネントを受け取る
    Animator animator;

    private bool IsDead;

    private bool _isRun = true;

    public int hp = 500;

    private bool isPlayerEnter;

    public EnemyWeapon ew;

    public GameObject Monster;

    Collider co;

    public GameObject WinPanel;
    AudioSource _audioSource;
    public AudioClip damageClip;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        co = ew.HitPointCollider;
        Run();

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
    }

    void Update()
    {
        agent.SetDestination(target.position);
    }

    IEnumerator DieCheck()
    {
        if (IsDead) yield break;
        if (hp <= 0)
        {
            Debug.LogWarning("DieCheck");
            Die();
            yield return new WaitForSeconds(3.0f);
            WinPanel.SetActive(true);
            yield return new WaitForSeconds(5.0f);
            SceneManager.LoadScene("Scenes/Build/TitleBuild");
        }
        
    }

    IEnumerator AttackLoop()
    {
        Debug.LogWarning("AttackLoop");
        if (IsDead) yield break;
        int count = 0;
        while (true)
        {
            Attack();
            count++;
            if(count >= 3)
            {
                DetermineSpeed(0);
                yield return new WaitForSeconds(5.0f);
                Run();
                break;
            }
            if (!isPlayerEnter)
            {
                Run();
                break;
            }
            yield return new WaitForSeconds(5.0f);
        }
    }

    public void DetermineSpeed(int speed)
    {
        agent.speed = speed;
    }

    public void Run()
    {
        if (IsDead) return;
        Debug.Log("Run");
        DetermineSpeed(10);
        animator.Play("Run");
    }

    public void Attack()
    {
        if (IsDead) return;
        animator.Play("Basic Attack");
    }

    public void Die()
    {
        animator.Play("Die");
        DetermineSpeed(0);
        IsDead = true;
    }

    public void Damage(int damage)
    {
        if (IsDead) return;
        _audioSource.clip = damageClip;
        _audioSource.Play();
        animator.Play("Get Hit");
        hp -= damage;
        StartCoroutine("DieCheck");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (IsDead) return;
        Debug.LogWarning("OntriggerEnter");
        isPlayerEnter = true;
        co.enabled = true;
        _isRun = false;
        animator.Play("Walk");
        DetermineSpeed(3);
        StartCoroutine("AttackLoop");
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if(other.gameObject.tag == "Player")
    //    {

    //    }
    //}

    private void OnTriggerExit(Collider other)
    {
        if (IsDead) return;
        Debug.LogWarning("OntriggerExit");
        _isRun = true;
        isPlayerEnter = false;
        Run();
        co.enabled = false;
    }

}
