﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WeaponSelect : MonoBehaviour {
    public AudioClip itemBtnClick;
    public AudioClip nextBtnClick;
    private AudioSource audioSource;
    public Weapon firstWeapon;
    public WeaponShowCase[] weaponShowCases;
    public BattleCharacter[] battleCharacters;
    public Transform startTransform;
    public Transform showTransform;
    public Transform exitTransform;
    public Transform playerTransform;
    public Text nameText;
    public SliderCtrl powerSlider;
    public float speed = 100;
    private int beforeSelectedWeapon = -1;
    private int nowSelectedWeapon = -1;
    private BattleCharacter playerBc;
    private Animator playerBcAnimator;

    // Use this for initialization
    void Start () {
        audioSource = gameObject.GetComponent<AudioSource>();
        PlayerSet();
        WeaponSelecter(firstWeapon,false);
    }
	
	// Update is called once per frame
	void Update () {
        if (nowSelectedWeapon >= 0)
        {
            GameObject selectedBig =
                 weaponShowCases[nowSelectedWeapon].bigWeapon;

            selectedBig.transform.position =
                Vector3.MoveTowards(selectedBig.transform.position, showTransform.position, Time.deltaTime * speed);

        }

        for(int i = 0; i < weaponShowCases.Length; i++)
        {
            if (i == nowSelectedWeapon) continue;
            if (beforeSelectedWeapon >= 0)
            {
                GameObject otherBig = weaponShowCases[i].bigWeapon;

                otherBig.transform.position
                    = Vector3.MoveTowards(otherBig.transform.position, exitTransform.position, Time.deltaTime * speed);
            }
        }
	}
    public void WeaponSelecter(Weapon we)
    {
        WeaponSelecter(we, true);
    }

    public void WeaponSelecter(Weapon we,bool audio)
    {
        if (nowSelectedWeapon == we.weponNum) return;

        if (audio)
        {
            audioSource.clip = itemBtnClick;
            audioSource.Play();
        }

        playerBc.WeaponAttach(we);
        playerBcAnimator.Play("Grounded");
        StartCoroutine(ActionNum(1));
       beforeSelectedWeapon = nowSelectedWeapon;
        nowSelectedWeapon = we.weponNum;

        weaponShowCases[nowSelectedWeapon].bigWeapon.transform.position = startTransform.position;
        Debug.Log(we.name);
        Debug.Log(we.power);
        Debug.Log(we.power / 100.0f);
        nameText.text = we.name;
        powerSlider.Hp = we.power / 100.0f;
    }

    private IEnumerator ActionNum(int v)
    {
        yield return new WaitForSeconds(0.1f);
        playerBcAnimator.SetInteger("ActionNum", v);
    }

    private void PlayerSet()
    {

        int num = PlayerPrefs.GetInt("SelectPlayerNum");
        Debug.Log("SelectPlayerNum" + num);
        for(int i=0;i< battleCharacters.Length; i++)
        {
            battleCharacters[i].gameObject.transform.position = playerTransform.position;
            battleCharacters[i].gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
            if (num == battleCharacters[i].characterNum)
            {
                playerBc = battleCharacters[i];
                playerBc.gameObject.SetActive(true);
                playerBcAnimator = playerBc.GetComponent<Animator>();
            }
        }
    }


    public void NextButtonClicked()
    {
        PlayerPrefs.SetInt("SelectWeaponNum", nowSelectedWeapon);
        audioSource.clip = nextBtnClick;
        audioSource.Play();
        StartCoroutine(SceneLoad());
    }

    private IEnumerator SceneLoad()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("Scenes/Build/StageSelectBuild");
    }

}
