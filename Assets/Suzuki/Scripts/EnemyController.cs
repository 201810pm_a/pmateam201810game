﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

    public Transform target;
    NavMeshAgent agent;
    Animator animator;
    public EnemyWeapon ew;
    bool isPlayerEnter;
    Collider co;
    public int Hp = 100;
    public GameObject win;
    private float agentSpeedInit;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agentSpeedInit = agent.speed;
        animator = GetComponent<Animator>();
        co = GetComponent<Collider>();
        co = ew.HitPointCollider;
        animator.SetBool("Walk", true);
        animator.ResetTrigger("Die");
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
    }

    void Update()
    {
        agent.SetDestination(target.position);
        if (Hp <= 0)
        {
            StartCoroutine("Wait");
        }
    }

     void OnTriggerEnter(Collider other)
    {
        if (Hp <= 0)
        {
            return;
        }
        isPlayerEnter = true;
        //agent.enabled = false;
        agent.speed = 0.2f;
        co.enabled = true;
        animator.SetBool("Walk", false);
        if(other.gameObject.tag == "Player")
        {
            StartCoroutine("AttackLoop");
        }    
    }
    void OnTriggerExit(Collider other)
    {
        if (Hp <= 0)
        {
            return;
        }
        //agent.enabled = true;
        agent.speed = agentSpeedInit;
        isPlayerEnter = false;
        co.enabled = false;
        animator.SetBool("Walk", true);
    }


    IEnumerator AttackLoop()
    {
        while (true)
        {
            
            if (!isPlayerEnter||Hp<=0)
            {
                break;
            }
            animator.Play("attack01");
            yield return new WaitForSeconds(3.0f);
        }
    }

    public void Damage(int power)
    {
        if (Hp <= 0)
        {
            return;
        }
        animator.Play("hit");
        Hp -= power;
    }

    IEnumerator Wait()
    {
        animator.SetTrigger("Die");
        yield return new WaitForSeconds(3.0f);
        win.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("Scenes/Build/TitleBuild");
    }
}
