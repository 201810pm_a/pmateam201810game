﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour {
    public AudioClip nextBtnClick;
    private AudioSource audioSource;
    // Use this for initialization

    void Start () {
        audioSource = gameObject.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update () {
		
	}
    public void PlayerSelectClicked()
    {
        audioSource.clip = nextBtnClick;
        audioSource.Play();
        StartCoroutine(SceneLoad());
    }


    private IEnumerator SceneLoad()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("Scenes/Build/PlayerSelectBuild");
    }
}
