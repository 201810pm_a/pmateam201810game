﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelect : MonoBehaviour {
    public GameObject CharacterPrefab;
    public GameObject InstantiateCube;
    public GameObject Cube;
    public GameObject DestroyCube;
    private GameObject obj;
	// Use this for initialization
    public void PictureClicked()
    {
        obj=Instantiate(CharacterPrefab,new Vector3(-65, 0, 0),Quaternion.Euler(0,120,0));
        obj.transform.position=Vector3.Lerp(InstantiateCube.transform.position, Cube.transform.position, 1.0f);
    }
    public void Decide()
    {
        string name = obj.name;
        switch (name)
        {
            case "1":
                PlayerPrefs.SetInt("CharacterNum", 1);
                break;
            case "2":
                PlayerPrefs.SetInt("CharacterNum", 2);
                break;
            case "3":
                PlayerPrefs.SetInt("CharacterNum", 3);
                break;
            case "4":
                PlayerPrefs.SetInt("CharacterNum", 4);
                break;
            case "5":
                PlayerPrefs.SetInt("CharacterNum", 5);
                break;
            case "6":
                PlayerPrefs.SetInt("CharacterNum", 6);
                break;
        }
        SceneManager.LoadScene("WeaponSelect");
    }
}
