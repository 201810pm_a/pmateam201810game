﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    Vector3 diff;
    Quaternion afterQuaternion;

    public GameObject target;
    public float followSpeed;
    public int directionNum = 0;
    private List<Vector3> directions;
    public float sideLength = 7;
    public float height = 7;
    // Use this for initialization
    void Start () {
        directions = new List<Vector3>();
        directions.Add(new Vector3(-sideLength, -height, 0));
        directions.Add(new Vector3(0, -height, sideLength));
        directions.Add(new Vector3(sideLength, -height, 0));
        directions.Add(new Vector3(0, -height, -sideLength));
        //diff = target.transform.position - transform.position;
        diff = directions[directionNum];
        transform.rotation = Quaternion.LookRotation(diff - Vector3.zero);
        afterQuaternion = transform.rotation;

    }
    public void ChangeDirection()
    {
        directionNum = (directionNum+1) % 4;
        diff = directions[directionNum];
        afterQuaternion = Quaternion.LookRotation(diff - Vector3.zero);

    }

    private void LateUpdate()
    {
        if (target)
        {
             transform.rotation = Quaternion.Lerp(
                transform.rotation,
                afterQuaternion,
                Time.deltaTime * followSpeed);

             transform.position = Vector3.Lerp(
                transform.position,
                target.transform.position - diff,
                Time.deltaTime * followSpeed
                );
        }
        else
        {
            Debug.Log("else else");
            Debug.Log(target);

        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
