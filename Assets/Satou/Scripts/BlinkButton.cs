﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class BlinkButton : MonoBehaviour
{
    Image image;
    Color color;
    float a_color;
    bool flag_G;
    public float speed = 3;
   int loop;
    // Use this for initialization
    void Start()
    {
        image = gameObject.GetComponent<Image>();
    }


    public void OnClick()
    {
        StartCoroutine(BlinkLoop());
    }

    private IEnumerator BlinkLoop()
    {
        yield return new WaitForSeconds(0.1f);
        if (image.enabled)
        {
            Debug.LogError("true");
            image.enabled = false;
        }
        else
        {
            Debug.LogError("false");
            image.enabled = true;
        }
        StartCoroutine(BlinkLoop());
    }
}