﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUIController : MonoBehaviour {
    public GameObject[] texts;
    public AudioClip nextBtnClick;
    private AudioSource audioSource;
    int textIndex;

	// Use this for initialization
	void Start () {
        audioSource = gameObject.GetComponent<AudioSource>();
        AllFalse();
        NextText();
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    void AllFalse()
    {
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].SetActive(false);
        }
    }

    public void NextText()
    {
        AllFalse();
        texts[textIndex].SetActive(true);
        audioSource.clip = nextBtnClick;
        audioSource.Play();
        textIndex = (textIndex + 1) % 8;
    }
}