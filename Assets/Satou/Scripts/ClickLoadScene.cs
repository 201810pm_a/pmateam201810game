﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickLoadScene : MonoBehaviour
{
    public string nextScene;
    public AudioClip nextBtnClick;
    private AudioSource audioSource;
    // Use this for initialization

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SceneLoad()
    {
        audioSource.clip = nextBtnClick;
        audioSource.Play();
        StartCoroutine(GoToScene());
    }


    private IEnumerator GoToScene()
    {
        yield return new WaitForSeconds(1.0f);
        Debug.Log("okkkk");
        SceneManager.LoadScene(nextScene);
    }
}
