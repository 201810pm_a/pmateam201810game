﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;


public class StageController : MonoBehaviour {
    private int prefsPlayerNum;
    private int prefsWeaponNum;
    public PlayerStatusBar hpSlider;
    public CameraFollow cameraFollow;
    public GameObject[] characters;
    public Weapon[] weapons;
    public Transform startTransform;
    private BattleCharacter playerCharacter;
    public Enemy[] enemys;
    private Weapon weapon;


    public bool IsMouseOnGUI
    {
        get
        {
            CustomThirdPersonUserControl cu = playerCharacter.GetComponent<CustomThirdPersonUserControl>();
            return cu.IsMouseOnGUI;
        }

        set
        {
           CustomThirdPersonUserControl cu = playerCharacter.GetComponent<CustomThirdPersonUserControl>();
            cu.IsMouseOnGUI = value;
        }
    }



    // Use this for initialization
    void Awake () {
        Debug.Log("SelectPlayerNum : " + PlayerPrefs.GetInt("SelectPlayerNum"));
        Debug.Log("SelectWeaponNum : " + PlayerPrefs.GetInt("SelectWeaponNum"));
        prefsPlayerNum = PlayerPrefs.GetInt("SelectPlayerNum");
        prefsWeaponNum = PlayerPrefs.GetInt("SelectWeaponNum");
        GameObject chara = Instantiate(characters[prefsPlayerNum],
            startTransform.position, Quaternion.identity);


        for(int i = 0; i < enemys.Length; i++)
        {
            enemys[i].target = chara;
        }


        playerCharacter = chara.GetComponent<BattleCharacter>();
        IsMouseOnGUI = false;
         //weapon = Instantiate(weapons[prefsWeaponNum],
        //    startTransform.position, Quaternion.identity);
        playerCharacter.WeaponAttach(weapons[prefsWeaponNum]);
        playerCharacter.HpSlidAttach(hpSlider);
        //playerCharacter.LosePanelAttach(losePanel);
        cameraFollow.target = playerCharacter.gameObject;
        Debug.Log("1111111111111111" + playerCharacter.gameObject);

        Debug.Log("22222222222222" + cameraFollow.target);

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Attack()
    {
        playerCharacter.Action(1);
    }

}
