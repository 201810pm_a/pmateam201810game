﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour {
    Func<BattleCharacter, bool> _attackCallBack;
    bool isStay;
    public Func<BattleCharacter, bool> AttackCallBack
    {
        get
        {
            return _attackCallBack;
        }

        set
        {
            _attackCallBack = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    IEnumerator AttackLoop(BattleCharacter bc)
    {
        while (true)
        {
            if (!isStay) break;
            AttackCallBack(bc);
            yield return new WaitForSeconds(3.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("AttackRange");
        if (other.gameObject.GetComponent<BattleCharacter>())
        {
            BattleCharacter bc = other.gameObject.GetComponent<BattleCharacter>();
            isStay = true;
            StartCoroutine("AttackLoop",bc);

            //SendMessage("Attack",1, SendMessageOptions.DontRequireReceiver);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<BattleCharacter>())
        {
             isStay = false;
         }
    }
}
