﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawMovingVector : MonoBehaviour {


    private int x = 0;
    private bool _down;
    Vector3 mouseClickPosition;
    Vector3 mouseMovePosition;
    bool isClick;
    public GameObject mouseClickIcon;
    public GameObject mouseMoveIcon;
    public GameObject mouseMiddleIcon;
    public GameObject instantiateLayer;
    private GameObject clickObj;
    private GameObject moveObj;
    private GameObject[] middleObj = new GameObject[3];
    private bool isMouseOnGUI;

    public bool IsMouseOnGUI
    {
        get
        {
            return isMouseOnGUI;
        }

        set
        {
           isMouseOnGUI = value;
        }
    }

    // Update is called once per frame
    // Use this for initialization
    void Start()
    {

    }
    private void FixedUpdate()
    {
        if (!isMouseOnGUI && Input.GetMouseButtonDown(0) && !isClick)
        {
            isClick = true;
            mouseClickPosition = Input.mousePosition;

            clickObj = Instantiate(mouseClickIcon, Input.mousePosition, Quaternion.identity);
            clickObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            clickObj.transform.SetParent(instantiateLayer.transform, true);
        }else if(Input.GetMouseButtonUp(0) && isClick)
        {
            isClick = false;
            Destroy(clickObj);
            for (int i = 0; i < middleObj.Length; i++)
            {
                Destroy(middleObj[i]);
            }
            Destroy(moveObj);
        }

        if (Input.GetMouseButton(0) && isClick)
        {

            mouseMovePosition = Input.mousePosition;
            Vector3 v = mouseClickPosition;
            for (int i=0;i< middleObj.Length; i++)
            {
                Destroy(middleObj[i]);
                v = mouseClickPosition + (mouseMovePosition - mouseClickPosition) / (middleObj.Length+1) * (i+1);
                middleObj[i] = Instantiate(mouseMiddleIcon, v, Quaternion.identity);
                middleObj[i].transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                middleObj[i].transform.SetParent(instantiateLayer.transform, true);
            }

            Destroy(moveObj);
 
            moveObj = Instantiate(mouseMoveIcon, Input.mousePosition, Quaternion.identity);
            moveObj.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            moveObj.transform.SetParent(instantiateLayer.transform, true);

            float dx = mouseMovePosition.x - mouseClickPosition.x;
            float dy = mouseMovePosition.y - mouseClickPosition.y;
            float deg = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
            moveObj.transform.rotation = Quaternion.Euler(new Vector3(0,0, deg));
            //moveObj.rec= Quaternion.LookRotation(clickObj.transform.position - moveObj.transform.position);

        }
    }
}
