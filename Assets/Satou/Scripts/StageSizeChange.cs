﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSizeChange : MonoBehaviour {
    public int width;
    public int height;
    // Use this for initialization
    void Start () {
        //スクリーンサイズ自動変更
        Screen.SetResolution(width, height, false, 60);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
