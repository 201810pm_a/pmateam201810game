﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour {

	
	void Start () {
		
	}

    // Update is called once per frame
    Vector3 mouseClickPosition;
    Vector3 mouseMovePosition;
    bool isClick;

	void Update () {

        if (Input.GetMouseButtonDown(0) && !isClick)
        {
            isClick = true;
            mouseClickPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0) && isClick)
        {
            isClick = false;
            mouseClickPosition = Vector3.zero;
        }

        if (Input.GetMouseButton(0) && isClick)
        {
            mouseMovePosition = Input.mousePosition;
            Debug.LogError("mouseMovePosition : " + mouseMovePosition);
            Debug.LogError("mouseClickPosition : " + mouseClickPosition);
            Debug.LogError("目標 : " + (mouseMovePosition - mouseClickPosition));
        }


    }
}
