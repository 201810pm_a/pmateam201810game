﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MainSoundScript : MonoBehaviour
{
    public bool DontDestroyEnabled = true;
    public AudioSource _audioSource;
    // Use this for initialization
    void Start()
    {
        if (DontDestroyEnabled)
        {
            if (GameObject.FindGameObjectsWithTag("MainSound").Length <= 1){
                _audioSource = gameObject.GetComponent<AudioSource>();
                _audioSource.enabled = true;
                // Sceneを遷移してもオブジェクトが消えないようにする
                DontDestroyOnLoad(this);
            }

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}