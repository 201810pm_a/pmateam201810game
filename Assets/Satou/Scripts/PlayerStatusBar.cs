﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // ←※これを忘れずに入れる
using UnityEngine.SceneManagement;

public class PlayerStatusBar : MonoBehaviour
{

    Slider _slider;
    public GameObject losePanel;
    void Start()
    {
        // スライダーを取得する
        _slider = GetComponent<Slider>();
    }

    float _nowHp = 1.0f;
    float _num = 1.0f;

    public float Num
    {
        get
        {
            return _num;
        }

        set
        {
            _num = value;
            _slider.value = _num;
            if (_slider.value <= 0.0f)
            {
                StartCoroutine("Wait");
                
            }
        }
    }

    IEnumerator Wait()
    {
         yield return new WaitForSeconds(3.0f);
        losePanel.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("Scenes/Build/TitleBuild");
    }

    void Update()
    {


    }
}
