﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCase : MonoBehaviour {
    public GameObject bigCharacter;
    public GameObject smallGameObject;
    public GameObject bigGameObject;
    private Animator bigAnimator;
    private Animator smallAnimator;

    public Animator BigAnimator
    {
        get
        {
            return bigAnimator;
        }

        set
        {
            bigAnimator = value;
        }
    }

    public Animator SmallAnimator
    {
        get
        {
            return smallAnimator;
        }

        set
        {
            smallAnimator = value;
        }
    }

    // Use this for initialization
    void Start () {
        SmallAnimator = smallGameObject.GetComponent<Animator>();
        BigAnimator = bigGameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
