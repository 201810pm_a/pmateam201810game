﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSelectContoller : MonoBehaviour {
    // Use this for initialization
    public AudioClip itemBtnClick;
    public AudioClip nextBtnClick;
    private AudioSource audioSource;
    public BattleCharacter firstBattleCharacter;
    public ShowCase[] showCases;
    public Transform startTransform;
    public Transform showTransform;
    public Transform exitTransform;
    public SliderCtrl hpSlider;
    public SliderCtrl speedSlider;
    public SliderCtrl powerSlider;
    public float speed = 100;
    private int beforeSelectedPlayer = -1;
    private int nowSelectedPlayer = -1;


    void Start () {
        audioSource = gameObject.GetComponent<AudioSource>();
        CharacterSelect(firstBattleCharacter, false);
    }
	
	// Update is called once per frame
	void Update () {
		if(nowSelectedPlayer >= 0)
        {
            GameObject selectedBig =
                showCases[nowSelectedPlayer].bigCharacter;

            selectedBig.transform.position
                = Vector3.MoveTowards(selectedBig.transform.position, showTransform.position, Time.deltaTime * speed);


        }

        for (int i=0;i< showCases.Length;i++) {
            if (i == nowSelectedPlayer) continue;
            if (beforeSelectedPlayer >= 0)
                {
                GameObject otherBig =
                    showCases[i].bigCharacter;

                otherBig.transform.position
                    = Vector3.MoveTowards(otherBig.transform.position, exitTransform.position, Time.deltaTime * speed);

            }
        }

    }

    public void CharacterSelect(BattleCharacter bc)
    {
        CharacterSelect(bc, true);
    }

    public void CharacterSelect(BattleCharacter bc, bool audio)
    {
        if (nowSelectedPlayer == bc.characterNum) return;
        beforeSelectedPlayer = nowSelectedPlayer;
        nowSelectedPlayer = bc.characterNum;

        if (audio)
        {
            audioSource.clip = itemBtnClick;
            audioSource.Play();
        }

        showCases[nowSelectedPlayer].bigCharacter.transform.position = startTransform.position;

        showCases[nowSelectedPlayer].SmallAnimator.Play("LobbyClick");
        showCases[nowSelectedPlayer].BigAnimator.Play("LobbyBigPose");
        
        Debug.Log(bc.hp);
        Debug.Log(bc.speed);
        Debug.Log(bc.hp / 100.0f);
        Debug.Log(bc.speed / 100.0f);
        hpSlider.Hp = bc.hp / 100.0f;
        speedSlider.Hp = bc.speed / 100.0f;
        powerSlider.Hp = bc.power / 100.0f;
    }

    public void NextClicked()
    {

        switch (nowSelectedPlayer)
        {
            case 0:
                PlayerPrefs.SetInt("SelectPlayerNum", nowSelectedPlayer);
                Debug.Log("キャラクター番号" + nowSelectedPlayer + "選択");
                break;
            case 1:
                PlayerPrefs.SetInt("SelectPlayerNum", nowSelectedPlayer);
                Debug.Log("キャラクター番号" + nowSelectedPlayer + "選択");
                break;
            case 2:
                PlayerPrefs.SetInt("SelectPlayerNum", nowSelectedPlayer);
                Debug.Log("キャラクター番号" + nowSelectedPlayer + "選択");
                break;
            case 3:
                PlayerPrefs.SetInt("SelectPlayerNum", nowSelectedPlayer);
                Debug.Log("キャラクター番号" + nowSelectedPlayer + "選択");
                break;
            case 4:
                PlayerPrefs.SetInt("SelectPlayerNum", nowSelectedPlayer);
                Debug.Log("キャラクター番号" + nowSelectedPlayer + "選択");
                break;
            case 5:
                PlayerPrefs.SetInt("SelectPlayerNum", nowSelectedPlayer);
                Debug.Log("キャラクター番号" + nowSelectedPlayer + "選択");
                break;
        }
        audioSource.clip = nextBtnClick;
        audioSource.Play();
        StartCoroutine(SceneLoad());
     }

    private IEnumerator SceneLoad()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("Scenes/Build/WeaponSelectBuild");
    }
}
