﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // ←※これを忘れずに入れる

public class SliderCtrl : MonoBehaviour
{

    Slider _slider;
    void Start()
    {
        // スライダーを取得する
        _slider = GetComponent<Slider>();
    }

    float _nowHp = 0;
    float _hp = 0.7f;

    public float Hp
    {
        get
        {
            return _hp;
        }

        set
        {
            _nowHp = 0;
            _hp = value;
        }
    }

    void Update()
    {
        _nowHp += 0.03f;
        if (_nowHp < Hp)
        {
            _slider.value = _nowHp;
       }

   }
}
