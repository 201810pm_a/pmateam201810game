﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour {
    private Collider _hitPointCollider;

 
    public List<GameObject> effects;
    /*

     public enum WeaponType
     {
         Sword = 1,
         Spear = 2
     }
     public WeaponType Type;
     */

    public Collider HitPointCollider
    {
        get
        {
            return _hitPointCollider;
        }

    }

    private void Awake()
    {
        _hitPointCollider = GetComponent<Collider>();
        _hitPointCollider.enabled = false;
    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
		
	}



    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Weapon.OnTriggerEnter()");
        if (other.gameObject.GetComponent<BattleCharacter>())
        {
            Debug.LogError("OnTriggerEnter innnnnnnnnn");
            BattleCharacter bc = other.gameObject.GetComponent<BattleCharacter>();
            bc.Damage(5);
        }
    }



}
