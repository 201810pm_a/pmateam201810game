﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {
    Animator _animator;
    NavMeshAgent _navMeshAgent;
    public GameObject target;
    AudioSource _audioSource;
    bool _dieFlag;
    Collider _collider;
    public EnemyWeapon _weapon;
    public AttackRange attackRange;
    public AudioClip damageClip;
    public int hp;
    public GameObject win;
    private bool _die;

    private void Awake()
    {

        attackRange.AttackCallBack = (bc) =>
        {
            Attack(bc);
            return true;
        };
        _audioSource = GetComponent<AudioSource>();
        _collider = GetComponent<Collider>();
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.enabled = false;
    }

    void SetAgentSpeed(float f)
    {
        Debug.Log("pppppppppppppppppp");
        if (!_animator.GetBool("Sleep"))
        {
            _navMeshAgent.speed = f;
        }
        
    }

    private void Attack(BattleCharacter bc)
    {
        Debug.Log("Attack(BattleCharacter bc)");
        if (!_animator.GetBool("Sleep") && !_die)
        {
            _animator.Play("Attack1");
        }
    }



    // Use this for initialization
    void Start () {
        _navMeshAgent.enabled = true;
        StartCoroutine("Sleep");
    }

    IEnumerator Sleep()
    {
        while (true)
        {
            if (_animator.GetBool("Sleep"))
            {
                yield return new WaitForSeconds(10.0f);
                _animator.SetBool("Sleep", false);
                _navMeshAgent.speed = 8;
            }
            else
            {
                yield return new WaitForSeconds(10.0f);
                _animator.SetBool("Sleep", true);
                _navMeshAgent.speed = 0;
                Debug.Log("pppppppppppppppppp");


            }
        }
    }


    // Update is called once per frame
    void Update () {
        // NavMeshが準備できているなら
        if (_navMeshAgent.pathStatus != NavMeshPathStatus.PathInvalid && target != null)
        {
            // NavMeshAgentに目的地をセット
            _navMeshAgent.SetDestination(target.transform.position);
        }
    }

    public void Damage(int power)
    {
        if (_die) return;
        _animator.Play("Damage");

        _audioSource.clip = damageClip;
        _audioSource.Play();
        hp -= 20;
        if(hp <= 0)
        {
            StartCoroutine("Wait");
        }

    }

    IEnumerator Wait()
    {
        _animator.Play("Die");
        _die = true;
        yield return new WaitForSeconds(5.0f);
        win.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("Scenes/Build/TitleBuild");
    }

    private void Die()
    {
        if (_dieFlag == false)
        {
            _dieFlag = true;
            _collider.enabled = false;
            DieEffect();
            _animator.SetBool("Die", true);
            _audioSource.Play();
            Debug.LogError("アニメ");
            StartCoroutine(DestroyObject());
        }
    }

    private IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }

    public virtual void DieEffect()
    {
        Debug.Log("Enemy DieEffect");
    }


    void Hit()
    {
        Debug.LogError("Hitttttttttt");
    }
    void FootL()
    {
        Debug.LogError("FootL");
    }
    void FootR()
    {
        Debug.LogError("FootR");
    }

    void HitStart()
    {
        Debug.LogError("HitStart");
        _weapon.HitPointCollider.enabled = true;
    }

    void HitEnd()
    {
        Debug.LogError("HitEnd");
        _weapon.HitPointCollider.enabled = false;
    }

    void Effect(int num)
    {
        Debug.LogError("num  " + num);
        GameObject effect = Instantiate(
            _weapon.effects[num],
            _weapon.transform.position,
            Quaternion.identity);

    }
}
