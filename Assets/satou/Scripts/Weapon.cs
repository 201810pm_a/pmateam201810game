﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    Collider _hitPointCollider;

    public int weponNum;
    public string weponName;
    public int power;
    public GameObject damageEffect;
    public enum WeaponType
    {
        Sword = 1,
        Spear = 2,
        Gun = 3
    }
    public WeaponType Type;

    public enum AttributeType
    {
        Fire = 1,
        Water = 2,
        Thunder = 3
    }
    public AttributeType attrType;

    public string GetAttrString()
    {
        switch (attrType)
        {
            case AttributeType.Fire:
                return "火";
             case AttributeType.Water:
                return "水";
            case AttributeType.Thunder:
                return "雷";
            default:
                return "エラー";
        }
    }


    public Transform handle;
    public List<GameObject> effects;

    public Bullet bullet;

    public Collider HitPointCollider
    {
        get
        {
            return _hitPointCollider;
        }

    }



    private void Awake()
    {
        _hitPointCollider = GetComponent<Collider>();
        _hitPointCollider.enabled = false;
    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
		
	}


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Weapon.OnTriggerEnter()");
        if (other.gameObject.GetComponent<Enemy>())
        {
             Enemy enemy = other.gameObject.GetComponent<Enemy>();
             GameObject effect = Instantiate(
                damageEffect,
                transform.position,
                Quaternion.identity);
            enemy.Damage(15);
        }
        else if(other.gameObject.GetComponent<EnemyController>())
        {
            GameObject effect = Instantiate(
               damageEffect,
               transform.position,
               Quaternion.identity);
            EnemyController enemy = other.gameObject.GetComponent<EnemyController>();
            enemy.Damage(15);
        }else if (other.gameObject.GetComponent<MonsterController>())
        {
            GameObject effect = Instantiate(
               damageEffect,
               transform.position,
               Quaternion.identity);
            MonsterController enemy = other.gameObject.GetComponent<MonsterController>();
            enemy.Damage(15);
        }
    }



}
