﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject effect;

    // Use this for initialization
    void Start () {
        StartCoroutine(DestroyObject());
    }
	
	// Update is called once per frame
	void Update () {

    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Weapon.OnTriggerEnter()");
        if (other.gameObject.GetComponent<Enemy>())
        {
            Debug.LogError("OnTriggerEnter innnnnnnnnn");
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            enemy.Damage(15);
            Destroy(this.gameObject);
        }else if (other.gameObject.GetComponent<EnemyController>())
        {
            Debug.LogError("OnTriggerEnter innnnnnnnnn");
            EnemyController enemy = other.gameObject.GetComponent<EnemyController>();
            enemy.Damage(15);
            Destroy(this.gameObject);
        }
        else if (other.gameObject.GetComponent<MonsterController>())
        {
            MonsterController enemy = other.gameObject.GetComponent<MonsterController>();
            enemy.Damage(15);
            Destroy(this.gameObject);
        }

    }


    private IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(4.0f);
        Destroy(this.gameObject);
    }
}
