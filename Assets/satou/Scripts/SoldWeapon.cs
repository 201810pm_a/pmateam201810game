﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldWeapon : MonoBehaviour {
    // Use this for initialization
    Weapon _weapon;
    Collider _hitPointCollider;

    void Start () {
        _weapon = GetComponent<Weapon>();
        _hitPointCollider = GetComponent<Collider>();
        _hitPointCollider.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BattleCharacter>())
        {
            BattleCharacter bc = other.gameObject.GetComponent<BattleCharacter>();
            bc.WeaponAttach(_weapon);
        }
    }
}
