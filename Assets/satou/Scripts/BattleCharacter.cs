﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.SceneManagement;
public class BattleCharacter : MonoBehaviour
{
    public AudioClip swordGesture;
    public AudioClip swordHit;
    public AudioClip spearGesture;
    public AudioClip spearHit;
    public AudioClip gunGesture;
    public AudioClip damage;
    public AudioClip gunHit;
    public int characterNum;
    public float speed = 1.5f;
    public int power = 64;
    public int hp = 30;
    //public String characterName;
    public Transform swordHandle;
    public Transform spearHandle;
    public Transform gunHandle;
    private Transform _handle;
    public Weapon weaponPrefab;
    public GameObject damageEffect;
    private AudioSource _audioSource;
    private PlayerStatusBar _sliderCtrl;
    Weapon _weapon;
    Animator _animator;
    private GameObject losePanel;
    bool _isDie;

    //delegate void SomeDelegate(Func<string, bool> func);
    Func<string, bool> _callback;
    int _callbackFrame;

    public Weapon Weapon
    {
        get
        {
            return _weapon;
        }

        set
        {
            _weapon = value;
        }
    }

    // Use this for initialization
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();

        if (weaponPrefab != null)
        {
            WeaponAttach(weaponPrefab);
        }
    }
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        // _weapon.transform.position -= (_weapon.handle.transform.position - _handle.transform.position);
        //_weapon.transform.position = _weapon.transform.TransformPoint(_weapon.handle.transform.localPosition * -1.0f);

        // LateCallback();
    }

    internal void Damage(int v)
    {
        if (_isDie) return;
        _animator.SetTrigger("Damage");
        GameObject effect = Instantiate(
            damageEffect,
            transform.position + new Vector3(0,1,0),
            Quaternion.identity);

        _audioSource.clip = damage;
        _audioSource.Play();
        if (_sliderCtrl)
        {
            Debug.LogWarning("v : " + v);
            _sliderCtrl.Num = (float)_sliderCtrl.Num- v/(float)hp;
            if(_sliderCtrl.Num <= 0.0f)
            {
                _isDie = true;
                _animator.SetBool("Die",true);
                _animator.Play("ddd");
                Debug.Log("ddd");
            }
        }
        else
        {
            Debug.LogWarning("_sliderCtrl : ない");
        }
    }



    void LateCallback()
    {
        if (_callback != null)
        {
            if (_callbackFrame > 0)
            {
                _callback(null);
                _callback = null;
                _callbackFrame = 0;
            }
            else
            {
                _callbackFrame++;
            }

        }
    }

    public void HpSlidAttach(PlayerStatusBar sc)
    {
        _sliderCtrl = sc;
    }

    
    public void WeaponAttach(Weapon wp)
    {
        //Debug.LogError("wp : " + wp);
        if (_weapon != null)
        {
            Destroy(_weapon.gameObject);
            _weapon = null;
        }

        if (_weapon == null)
        {
            //Debug.LogError("nullllllllllllllll");
        }

        _handle = GetHandle((int)wp.Type);
        _weapon = (Weapon)Instantiate(
            wp,
            _handle.position,
            _handle.rotation);

        _weapon.HitPointCollider.enabled = false;

        _animator.SetInteger("WeaponType", (int)_weapon.Type);


        _weapon.transform.parent = _handle.transform;
        _weapon.handle.transform.parent = _weapon.transform;
        _weapon.transform.localPosition = _weapon.handle.transform.localPosition * -1.0f / transform.localScale.y;
         _weapon.transform.localRotation = _weapon.handle.transform.localRotation;

    }

    private Transform GetHandle(int type)
    {
        switch (type)
        {
            case 1:
                return swordHandle;
            case 2:
                return spearHandle;
            case 3:
                return gunHandle;
            default:
                return null;
        }
    }

    internal void AttackTest()
    {
        throw new NotImplementedException();
    }

    public void Action(int actionNum)
    {
        Debug.LogError("xxx ActionNum : " + actionNum);
        _animator.SetInteger("ActionNum", actionNum);
        _animator.SetInteger("MultiAction", _animator.GetInteger("MultiAction") + 1);
    }

    private IEnumerator RunLate(Func<string, bool> callback)
    {
        throw new NotImplementedException();
    }


    void Hit()
    {
        Debug.LogError("Hitttttttttt");
    }
    void FootL()
    {
        Debug.LogError("FootL");
    }
    void FootR()
    {
        Debug.LogError("FootR");
    }

    void HitStart()
    {
        Debug.LogError("HitStart");
        _weapon.HitPointCollider.enabled = true;
        _audioSource.clip = swordGesture;
        _audioSource.Play();
    }

    void HitEnd()
    {
        Debug.LogError("HitEnd");
        _weapon.HitPointCollider.enabled = false;
    }

    void GunShootSingle(int num)
    {
        Debug.LogError("GunShootSingle");
        if ((int)_weapon.Type == 3 && _weapon.bullet != null)
        {
            Bullet bullet = Instantiate(
            _weapon.bullet,
            _weapon.transform.position,
            _weapon.transform.rotation);

            Vector3 pv = transform.forward * 100;
            //プレイヤーのrigidbodyに↑の数値分だけ力を加える。
            Rigidbody rigidbody= bullet.gameObject.GetComponent<Rigidbody>();
            rigidbody.AddForce(pv, ForceMode.VelocityChange);
            _audioSource.clip = gunGesture;
            _audioSource.Play();
            if (num == 1 && bullet.effect != null)
            {
                GameObject ef = Instantiate(
                 bullet.effect,
                 _weapon.transform.position,
                 _weapon.transform.rotation);
                ef.transform.parent = bullet.transform;

            }
        }

    }

    void Effect(int num)
    {
        Debug.LogError("num  " + num);
        GameObject effect = Instantiate(
            _weapon.effects[num],
            _weapon.transform.position,
            Quaternion.identity);

    }

}