﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDownController : MonoBehaviour
{

    //プレイヤーオブジェクトをアタッチ
    public GameObject player;
    //初期座標
    Vector3 startPos;


    // Use this for initialization
    void Start()
    {
        //プレイヤーの初期座標を格納
        startPos = player.transform.position + new Vector3(0,10,0);
    }

    // Update is called once per frame
    void Update()
    {

    }


    //FallDownオブジェクト接触時の処理
    private void OnTriggerEnter(Collider other)
    {
        //プレイヤーが落下時
        if (other.gameObject.tag == "Player")
        {
            //スタート地点に戻る
            other.gameObject.transform.position = startPos;
        }else if (other.gameObject.tag == "PlayerPart")
        {
            // なにもしない
        }
        else
        {
            //プレイヤー以外のオブジェクトが落下した際はオブジェクトを消去
            Destroy(other.gameObject);
        }
    }
}

